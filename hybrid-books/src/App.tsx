import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.css'
import BookCards from './components/Book/BookCards'
import EditBook from './components/EditBook/EditBook'
import Login from './components/Login/Login'

function App () {
  const UNAUTHORIZED = 1
  const USER = 2
  const ADMIN = 3
  let role = UNAUTHORIZED
  const rolesString = localStorage.getItem('roles') || ''
  const roles = rolesString ? rolesString.split(',') : null
  if (!roles) {
    role = UNAUTHORIZED
  } else if (roles.length > 1) {
    role = ADMIN
  } else if (roles.length > 0) {
    role = USER
  }
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {role >= USER && (<Route path='/' element={<BookCards/>}/>)}
          {role === ADMIN && (<Route path='/edit/:id' element={<EditBook/>}/>)}
          {role === ADMIN && (<Route path='/create' element={<EditBook/>}/>)}
          <Route path='/login' element={<Login/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
