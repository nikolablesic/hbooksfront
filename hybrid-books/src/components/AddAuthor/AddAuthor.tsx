/* eslint-disable no-unused-vars */
import React, { useEffect, useRef, useState } from 'react'
import { Author } from '../../model/Author'
import { authorCreate } from '../../store/actions/book.action'
import { useAppDispatch } from '../../store/hooks'
import './AddAuthor.css'

const AddAuthor = (props: { onClickOutside: any }) => {
  const ref = useRef<HTMLDivElement>(null)
  const { onClickOutside } = props
  const [firstName, setFirstName] = useState<string>('')
  const [middleName, setMiddleName] = useState<string>('')
  const [lastName, setLastName] = useState<string>('')

  const dispatch = useAppDispatch()

  const submit = (event:any) => {
    event.preventDefault()
    const author = {
      firstName,
      middleName,
      lastName
    } as Author
    onClickOutside()
    dispatch(authorCreate(author))
  }
  const handleChangeInput = (event:any) => {
    const name = event.target.name
    const value = event.target.value
    switch (name) {
      case 'firstName':
        setFirstName(value)
        break
      case 'middleName':
        setMiddleName(value)
        break
      case 'lastName':
        setLastName(value)
        break
      default:
        break
    }
  }
  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (ref.current && !ref.current.contains(event.target)) {
        onClickOutside && onClickOutside()
      }
    }
    document.addEventListener('click', handleClickOutside, true)
    return () => {
      document.removeEventListener('click', handleClickOutside, true)
    }
  }, [onClickOutside])

  return (
    <div ref={ref}>
      <div className='author-card'>
            <h2>Add author</h2>
            <form className='author-card__form' onSubmit={submit}>
                First Name
                <input name='firstName' className='author-card__input' type="text" placeholder='First name' value={firstName}
                onChange={handleChangeInput} required/>
                Middle Name
                <input name='middleName' className='author-card__input' type="text" placeholder='Middle name' value={middleName}
                onChange={handleChangeInput} required/>
                Last Name
                <input name='lastName' className='author-card__input' type="text" placeholder='Last name' value={lastName}
                onChange={handleChangeInput} required/>
                <input className='author-card__submit' type="submit" value="SAVE"/>
            </form>
        </div>
    </div>
  )
}
export default AddAuthor
