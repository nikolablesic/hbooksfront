import React, { useState } from 'react'
import { Book } from '../../model/Book'
import { bookRent } from '../../store/actions/book.action'
import { useAppDispatch } from '../../store/hooks'
import Dropdown from '../Dropdown/Dropdown'
import './BookCards.css'

const BookCard = (props: { book: Book, index: number}) => {
  const [isExpanded, setIsExpanded] = useState(-1)
  const dispatch = useAppDispatch()
  const rent = () => {
    if (props.book.id) {
      dispatch(bookRent(props.book.id))
    }
  }
  return (
        <span>
            <div className='book-image'>
                <img src={props.book.imageUrl} onError={({ currentTarget }) => {
                  currentTarget.onerror = null
                  currentTarget.src = '/assets/coverNotAvailable.png'
                }}/>
                </div>
                <div className='book-details'>
                <Dropdown bookId={props.book.id}/>
                <h2>{props.book.title}</h2>
                <div className='details-container'>
                    <div className='details'>
                        <b>Description: </b>{props.book.description.length > 30 && isExpanded !== props.index ? props.book.description.substring(0, 30) : props.book.description}
                        <span className='readButton'>{props.book.description.length > 30 && isExpanded !== props.index ? <span onClick={() => setIsExpanded(props.index)}> Read more</span> : ''}</span>
                        <span className='readButton'>{isExpanded === props.index ? <span onClick={() => setIsExpanded(-1)}> Read less</span> : ''}</span><br/><br/>
                        <b>Authors: </b><i>{props.book.authors?.map((author, index) =>
                          (<span key={author.id}>{author.firstName} {author.middleName} {author.lastName}
                          {(props.book.authors && props.book.authors?.length - 1 > index) ? <span>, </span> : <></>}</span>)
                        )}</i><br/><br/>
                        <b>Available copies:</b> {props.book.quantity}<b/>
                    </div>
                    <input className='rent-button' type='button' value='RENT' disabled={!props.book.quantity || props.book.quantity <= 0} onClick={rent}/>
              </div>
            </div>
        </span>
  )
}
export default BookCard
