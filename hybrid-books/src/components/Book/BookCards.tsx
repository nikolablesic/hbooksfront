import React, { useCallback, useEffect, useRef, useState } from 'react'
import { getAllBooks } from '../../store/actions/book.action'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import { BookState } from '../../store/reducers/book.reducer'
import Header from '../Header/Header'
import BookCard from './BookCard'
import './BookCards.css'

const BookCards = () => {
  const dispatch = useAppDispatch()

  const { books, hasMore } = useAppSelector<BookState>(state => state.books)

  const [query, setQuery] = useState<string>('')
  const [pageNumber, setPageNumber] = useState(0)
  const observer = useRef<IntersectionObserver>()

  const lastBookElementRef = useCallback((node: any) => {
    if (observer.current) observer.current.disconnect()
    observer.current = new IntersectionObserver(entries => {
      if (entries[0].isIntersecting && hasMore) {
        setPageNumber(prevPageNumber => prevPageNumber + 1)
      }
    })
    if (node) observer.current.observe(node)
  }, [hasMore])

  const prevPageNumber = useRef(pageNumber)

  useEffect(() => {
    if (prevPageNumber.current === pageNumber) {
      dispatch(getAllBooks(query, pageNumber, false))
    } else {
      dispatch(getAllBooks(query, pageNumber, true))
    }
  }, [query, pageNumber])

  const handleChange = (e: any) => {
    setQuery(e.target.value)
    setPageNumber(0)
  }

  return (
    <div>
      <Header/>
      <input className='search-box' type="text" placeholder="Search.." value={query} onChange={handleChange}/>
      <i className="fa fa-search search-button"></i>
      <div className='cards'>
        {books.map((book, index) => {
          if (books.length === index + 1) {
            return <div ref={lastBookElementRef} className='book-card' key={book.id}>
                    <BookCard book={book} index={index}></BookCard>
                  </div>
          } else {
            return <div className='book-card' key={book.id}>
                    <BookCard book={book} index={index}></BookCard>
                  </div>
          }
        }
        )}
      </div>
      </div>
  )
}
export default BookCards
