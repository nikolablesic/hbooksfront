import React from 'react'
import toast, { Toaster } from 'react-hot-toast'
import { bookDelete } from '../../store/actions/book.action'
import { useAppDispatch } from '../../store/hooks'
import './Dropdown.css'

const Dropdown = (props: { bookId: number | undefined }) => {
  const dispatch = useAppDispatch()
  const deleteBook = () => {
    if (props.bookId) {
      dispatch(bookDelete(props.bookId)).then(() => {
        toast.error('Book is rented!')
      })
    }
  }
  return (
    <div>
      <Toaster position="top-center" reverseOrder={false}/>
      <div className="menu-nav">
      <div className="dropdown-container" tabIndex={-1}>
        <div className="three-dots"></div>
        <div className="dropdown">
          <a href={'/edit/' + props.bookId}><div>Edit</div></a>
          <a href="javascript:void(0)" onClick={deleteBook}><div>Delete</div></a>
        </div>
      </div>
    </div>
  </div>
  )
}
export default Dropdown
