import React, { useEffect, useState } from 'react'
import toast, { Toaster } from 'react-hot-toast'
import { useNavigate, useParams } from 'react-router-dom'
import { Author } from '../../model/Author'
import { Book } from '../../model/Book'
import { bookCreate, bookUpdate, getAllAuthors, getBook } from '../../store/actions/book.action'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import { BookState } from '../../store/reducers/book.reducer'
import AddAuthor from '../AddAuthor/AddAuthor'
import Footer from '../Footer/Footer'
import Header from '../Header/Header'
import './EditBook.css'

const EditBook = () => {
  const minLen5Regex = /^.{5,}$/
  const digitRegex = /^\d+$/

  const [title, setTitle] = useState<string>('')
  const [description, setDescription] = useState<string>('')
  const [isbn, setIsbn] = useState<string>('')
  const [quantityStr, setQuantityStr] = useState<string>('')
  const [imageUrl, setImageUrl] = useState<string>('')
  const [selectedAuthors, setSelectedAuthors] = useState<Set<Number>>(new Set<Number>())

  const [titleError, setTitleError] = useState<string>('')
  const [descriptionError, setDescriptionError] = useState<string>('')
  const [isbnError, setIsbnError] = useState<string>('')
  const [quantityStrError, setQuantityStrError] = useState<string>('')

  const [isDropDownActive, setDropDownActive] = useState(false)
  const [isAddAuthorActive, setAddAuthorActive] = useState(false)
  const toggleDropDown = () => {
    setDropDownActive(!isDropDownActive)
  }
  const toggleAddAuthor = () => {
    setAddAuthorActive(!isAddAuthorActive)
  }
  const { authors } = useAppSelector<BookState>(state => state.books)

  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const { book } = useAppSelector<BookState>(state => state.books)
  const { id } = useParams()
  useEffect(() => {
    dispatch(getAllAuthors())
    if (id) {
      dispatch(getBook(Number(id)))
    }
  }, [id])

  useEffect(() => {
    setTitle(book.title)
    setDescription(book.description)
    if (book.isbn) {
      setIsbn(book.isbn)
    }
    if (book.quantity) {
      setQuantityStr(book.quantity.toString())
    }
    if (book.imageUrl) {
      setImageUrl(book.imageUrl)
    }
    book.authors?.forEach(author => {
      selectedAuthors.add(author.id)
    })
  }, [book])

  const authorOnChange = (event:any, id: Number) => {
    if (event.target.checked === true) {
      selectedAuthors.add(id)
      setSelectedAuthors(new Set(selectedAuthors))
    } else {
      selectedAuthors.delete(id)
      setSelectedAuthors(new Set(selectedAuthors))
    }
  }

  const submit = (event:any) => {
    event.preventDefault()
    if (!isFormValid()) {
      return
    }
    const bookAuthors: Author[] = []
    authors.forEach(author => {
      if (selectedAuthors.has(author.id)) {
        bookAuthors.push(author)
      }
    })
    const quantity = Number(quantityStr)
    const updatedBook: Book = {
      id: book.id,
      title,
      description,
      isbn,
      quantity,
      imageUrl,
      authors: bookAuthors,
      creationDate: book.creationDate
    }
    if (id) {
      dispatch(bookUpdate(updatedBook)).then(() => {
        navigate('/')
      }).catch(() => {
        toast.error('Something went wrong, please try again!')
      })
    } else {
      dispatch(bookCreate(updatedBook)).then(() => {
        navigate('/')
      }).catch(() => {
        toast.error('Something went wrong, please try again!')
      })
    }
  }

  const isFormValid = (): boolean => {
    let ret = true
    if (!title) {
      setTitleError('Please fill in this field')
      ret = false
    } else if (!minLen5Regex.test(title)) {
      setTitleError('Title must contain at least 5 character')
      ret = false
    }
    if (!description) {
      setDescriptionError('Please fill in this field')
      ret = false
    } else if (!minLen5Regex.test(description)) {
      setDescriptionError('Description must contain at least 5 character')
      ret = false
    }
    if (!isbn) {
      setIsbnError('Please fill in this field')
      ret = false
    }
    if (!quantityStr) {
      setQuantityStrError('Please fill in this field')
      ret = false
    } else if (!digitRegex.test(quantityStr) || Number(quantityStr) < 0) {
      setQuantityStrError('Quantity must be a number')
      ret = false
    }
    return ret
  }

  return (
    <div>
      <Header/>
      <Toaster position="top-center" reverseOrder={false}/>
      <div className='edit-card'>
          {id ? <h2>Edit book</h2> : <h2>Create book</h2>}
          <form className='edit-card__form' onSubmit={submit}>
              <label className='edit-card__label'>Title</label>
              <input className='edit-card__input' type="text" placeholder='Title' value={title}
              onChange={event => { setTitle(event.target.value); setTitleError('') }}/>
              <p className='edit-card__error-message'>{titleError}</p>
              <label className='edit-card__label'>Description</label>
              <textarea rows={4} cols={50} className='edit-card__input' placeholder='Description' value={description}
              onChange={event => { setDescription(event.target.value); setDescriptionError('') }}>
              </textarea>
              <p className='edit-card__error-message'>{descriptionError}</p>
              <label className='edit-card__label'>ISBN</label>
              <input className='edit-card__input' type="text" placeholder='ISBN' value={isbn}
              onChange={event => { setIsbn(event.target.value); setIsbnError('') }}/>
              <p className='edit-card__error-message'>{isbnError}</p>
              <label className='edit-card__label'>Quantity</label>
              <input className='edit-card__input' type="number" placeholder='Quantity' value={quantityStr}
              onChange={event => { setQuantityStr(event.target.value); setQuantityStrError('') }}/>
              <p className='edit-card__error-message'>{quantityStrError}</p>
              <label className='edit-card__label'>Image URL</label>
              <input className='edit-card__input' type="text" placeholder='Image URL' value={imageUrl}
              onChange={event => { setImageUrl(event.target.value) }}/>
              <span className='edit-card__label'>Authors {'\u00A0'}<i className="fa fa-plus" onClick={toggleAddAuthor}></i></span>
              <div className={`edit-card__checkbox-dropdown ${isDropDownActive ? 'edit-card__is-active' : null}`} onClick={toggleDropDown}>
              Choose authors
              <ul className="edit-card__checkbox-dropdown-list" onClick={(event) => { event.stopPropagation() }}>
                  {authors.map((author, id) => (<li key={author.id}>
                    <div className='authors-select'>
                    <input type="checkbox" name="author" checked={selectedAuthors.has(author.id)} onChange={(e) => authorOnChange(e, author.id)}/>
                  <label>
                      {author.firstName} {author.middleName} {author.lastName}
                  </label>
                    </div>
                </li>))}
              </ul>
              </div>
              <input className='edit-card__submit' type="submit" value="SAVE"/>
          </form>
          {isAddAuthorActive ? <AddAuthor onClickOutside={() => setAddAuthorActive(!isAddAuthorActive)}></AddAuthor> : <></>}
      </div>
      <Footer/>
    </div>
  )
}
export default EditBook
