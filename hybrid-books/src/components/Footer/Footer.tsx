import React from 'react'
import './Footer.css'

const Footer = () => {
  return (
      <div className='footer'>
          &copy; 2022 Hybrid IT
      </div>
  )
}
export default Footer
