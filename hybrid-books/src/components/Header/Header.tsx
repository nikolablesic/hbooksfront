import React, { useEffect, useState } from 'react'
import { logout } from '../../services/service'
import { removeAllRoles } from '../../store/actions/auth.action'
import { useAppDispatch } from '../../store/hooks'
import './Header.css'

const Header = () => {
  const [isNavExpanded, setIsNavExpanded] = useState(false)
  const [roles, setRoles] = useState<string[]>([])

  const dispatch = useAppDispatch()

  useEffect(() => {
    const rolesString = localStorage.getItem('roles')
    if (rolesString) {
      setRoles(rolesString.split(','))
    }
  }, [])

  const logoutClick = () => {
    logout()
    dispatch(removeAllRoles())
  }
  return (
  <nav className="navigation">
        <a href="/" className="brand-name">
          Hybrid Books
        </a>
        <button
          className="hamburger"
          onClick={() => {
            setIsNavExpanded(!isNavExpanded)
          }}
        >
          {/* icon from Heroicons.com */}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="white"
          >
            <path
              fillRule="evenodd"
              d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM9 15a1 1 0 011-1h6a1 1 0 110 2h-6a1 1 0 01-1-1z"
              clipRule="evenodd"
            />
          </svg>
        </button>
        <div
          className={
            isNavExpanded ? 'navigation-menu expanded' : 'navigation-menu'
          }
        >
          <ul className='menu'>
            <li>
              <a href="/">Books</a>
            </li>
            { roles.length > 1
              ? <li>
              <a href="/create">Create book</a>
            </li>
              : <></>}
            <li>
              <a href="/login" onClick={logoutClick}>Log out</a>
            </li>
          </ul>
        </div>
      </nav>)
}
export default Header
