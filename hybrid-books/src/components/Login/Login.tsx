import React, { useState } from 'react'
import toast, { Toaster } from 'react-hot-toast'
import { login } from '../../services/service'
import { getAllRoles } from '../../store/actions/auth.action'
import { useAppDispatch } from '../../store/hooks'
import Footer from '../Footer/Footer'
import './Login.css'

const Login = () => {
  const [username, setUsername] = useState<string>('')
  const [password, setPassword] = useState<string>('')

  const [usernameError, setUsernameError] = useState<string>('')
  const [passwordError, setPasswordError] = useState<string>('')

  const usernameRegex = /^.{5,}$/
  const passwordRegex = /^.{5,}$/

  const dispatch = useAppDispatch()

  const submit = (event:any) => {
    event.preventDefault()
    if (!isFormValid()) {
      return
    }
    login(username, password).then(response => {
      localStorage.setItem('user', JSON.stringify(response.data))
      dispatch(getAllRoles())
      window.location.href = '/'
    }).catch(() => {
      toast.error('Bad credentials!')
    })
  }

  const isFormValid = (): boolean => {
    let ret = true
    if (!password) {
      setPasswordError('Please fill in this field')
      ret = false
    } else if (!passwordRegex.test(password)) {
      setPasswordError('Password must contain at least 5 character')
      ret = false
    }
    if (!username) {
      setUsernameError('Please fill in this field')
      ret = false
    } else if (!usernameRegex.test(username)) {
      setUsernameError('Username must contain at least 5 character')
      ret = false
    }
    return ret
  }

  return (
    <div>
      <div className='login-card'>
          <Toaster position="top-center" reverseOrder={false}/>
          <form onSubmit={submit}>
          <h2>Login</h2>
          <input name='username' className='login-card__username' type="text" placeholder='Username' value={username}
          onChange={event => { setUsername(event.target.value); setUsernameError('') }}/>
          <p className='login-card__error-message'>{usernameError}</p>
          <input name='password' className='login-card__password' type="password" placeholder='Password' value={password}
          onChange={event => { setPassword(event.target.value); setPasswordError('') }}/>
          <p className='login-card__error-message'>{passwordError}</p>
          <input className='login-card__submit' type="submit" value="LOGIN"/>
          </form>
      </div>
      <Footer/>
    </div>)
}
export default Login
