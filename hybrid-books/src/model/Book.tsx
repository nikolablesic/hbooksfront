import { Author } from './Author'

export interface Book {
  id?: number
  title: string
  authors?: Author[]
  description: string
  isbn?: string
  quantity: number
  imageUrl?: string
  creationDate?: string
}
