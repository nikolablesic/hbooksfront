import axios from 'axios'
// eslint-disable-next-line camelcase
import jwt_decode from 'jwt-decode'
import moment from 'moment'
import { Author } from '../model/Author'
import { Book } from '../model/Book'

const SERVER_URL = 'http://54.93.246.187:8080/api/v3'
const SERVER_URL_V1 = 'http://54.93.246.187:8080/api/v1'

const getBooks = async (query: string, page: number): Promise<Book[]> => {
  const response = await axios.get(SERVER_URL + '/books', {
    params: {
      title: query, description: query, isbn: query, quantity: query, firstName: query, middleName: query, lastName: query, page
    }
  })
  return response.data
}

const getBookById = async (id:number): Promise<Book> => {
  const response = await axios.get(SERVER_URL + '/books/' + id)
  return response.data
}

const getAuthors = async (): Promise<Author[]> => {
  const response = await axios.get(SERVER_URL_V1 + '/authors', { headers: { Authorization: authToken() } })
  return response.data
}

const createAuthor = async (author: Author) => {
  return axios.post(SERVER_URL + '/authors', {
    firstName: author.firstName,
    middleName: author.middleName,
    lastName: author.lastName
  }, { headers: { Authorization: authToken() } })
}

const updateBook = async (book: Book): Promise<Book> => {
  const response = await axios.put(SERVER_URL + '/books/' + book.id, {
    title: book.title,
    description: book.description,
    authors: book.authors?.map(author => {
      return {
        firstName: author.firstName,
        middleName: author.middleName,
        lastName: author.lastName
      }
    }),
    creationDate: book.creationDate,
    isbn: book.isbn,
    quantity: book.quantity,
    imageUrl: book.imageUrl
  }, { headers: { Authorization: authToken() } })
  return response.data
}

const createBook = async (book: Book) => {
  return axios.post(SERVER_URL + '/books', {
    title: book.title,
    description: book.description,
    authors: book.authors?.map(author => {
      return {
        firstName: author.firstName,
        middleName: author.middleName,
        lastName: author.lastName
      }
    }),
    creationDate: moment(new Date()).format('YYYY-MM-DD'),
    isbn: book.isbn,
    quantity: book.quantity,
    imageUrl: book.imageUrl
  }, { headers: { Authorization: authToken() } })
}

const deleteBook = async (id:number) => {
  return await axios.delete(SERVER_URL + '/books/' + id, { headers: { Authorization: authToken() } })
}

const rentBook = async (id:number) => {
  return await axios.post(SERVER_URL + '/rent', {
    bookId: id
  }, { headers: { Authorization: authToken() } })
}

const authToken = () => {
  const userLocalStorage = localStorage.getItem('user')
  if (!userLocalStorage) {
    return ''
  }
  const user = JSON.parse(userLocalStorage)
  if (user?.token) {
    return 'Bearer ' + user.token
  } else {
    return ''
  }
}

const decodeJWT = () => {
  const token = authToken()
  const decoded = jwt_decode(token)
  return decoded
}

export function logout () {
  localStorage.removeItem('user')
  localStorage.removeItem('roles')
}
export function login (username: string, password: string) {
  return axios.post(SERVER_URL + '/auth/login', {
    username,
    password
  })
}

export default {
  getBookById,
  getBooks,
  getAuthors,
  createAuthor,
  updateBook,
  createBook,
  deleteBook,
  authToken,
  decodeJWT,
  rentBook
}
