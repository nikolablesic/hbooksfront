import service from '../../services/service'
import { getRoles, removeRoles } from '../reducers/auth.reducer'

export const getAllRoles = () => async (dispatch: (arg0: { type: string; payload: string[] }) => void) => {
  const response = await service.decodeJWT()
  dispatch(getRoles(response))
}

export const removeAllRoles = () => async (dispatch: (arg0: { type: string; }) => void) => {
  dispatch(removeRoles())
}
