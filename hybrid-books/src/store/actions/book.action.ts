import { Author } from '../../model/Author'
import { Book } from '../../model/Book'
import service from '../../services/service'
import { createAuthor, createBook, deleteBook, getAuthors, getBookById, getBooks, rentBook, updateBook } from '../reducers/book.reducer'

export const getAllBooks = (query: string, page: number, concat: boolean) => async (dispatch: (arg0: { type: string; payload: Book[] }) => void) => {
  const response = await service.getBooks(query, page)
  dispatch(getBooks({
    response,
    concat,
    page
  }))
}

export const getBook = (id:number) => async (dispatch: (arg0: { type: string; payload: Book }) => void) => {
  const response = await service.getBookById(id)
  dispatch(getBookById(response))
}

export const getAllAuthors = () => async (dispatch: (arg0: { type: string; payload: Author[] }) => void) => {
  const response = await service.getAuthors()
  dispatch(getAuthors(response))
}

export const bookUpdate = (book: Book) => async (dispatch: (arg0: { type: string; payload: Book }) => void) => {
  const response = service.updateBook(book)
  dispatch(updateBook(response))
}

export const bookCreate = (book: Book) => async (dispatch: (arg0: { type: string; payload: Book }) => void) => {
  service.createBook(book).then((response) => {
    dispatch(createBook(response.data))
  })
}

export const authorCreate = (author: Author) => async (dispatch: (arg0: { type: string; payload: Author }) => void) => {
  service.createAuthor(author).then((response) => {
    dispatch(createAuthor(response.data))
  })
}

export const bookDelete = (id: number) => async (dispatch: (arg0: { type: string; payload: number }) => void) => {
  service.deleteBook(id).then(() => {
    dispatch(deleteBook(id))
  })
}

export const bookRent = (id: number) => async (dispatch: (arg0: { type: string; payload: number }) => void) => {
  service.rentBook(id).then(() => {
    dispatch(rentBook(id))
  })
}
