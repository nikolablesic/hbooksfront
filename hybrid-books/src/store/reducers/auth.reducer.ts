import { createSlice } from '@reduxjs/toolkit'

export interface AuthState {
  roles: string[]
}

const initialState = {
  roles: []
}

export const authReducer = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    getRoles (state, action) {
      state.roles = action.payload.role
      localStorage.setItem('roles', action.payload.role)
    },
    removeRoles (state) {
      state.roles = []
    }
  }
})

export const { getRoles, removeRoles } = authReducer.actions
export default authReducer.reducer
