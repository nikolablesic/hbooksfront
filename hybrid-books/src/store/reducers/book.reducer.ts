import { createSlice } from '@reduxjs/toolkit'
import { Author } from '../../model/Author'
import { Book } from '../../model/Book'

export interface BookState {
  books: Book[],
  book: Book,
  authors: Author[],
  hasMore: boolean
}

const initialState = {
  books: [] as Book[],
  book: {} as Book,
  authors: [] as Author[],
  hasMore: true
}

export const bookReducer = createSlice({
  name: 'books',
  initialState,
  reducers: {
    getBooks (state, action) {
      if (action.payload.response.length <= action.payload.page) {
        state.hasMore = false
      } else {
        state.hasMore = true
      }
      if (action.payload.concat) {
        state.books = [...state.books.concat(action.payload.response)]
      } else {
        state.books = action.payload.response
      }
    },
    getBookById (state, action) {
      state.book = action.payload
    },
    getAuthors (state, action) {
      state.authors = action.payload
    },
    updateBook (state, action) {
      state.books = state.books.map((item) => {
        if (item.id === action.payload.id) {
          return action.payload
        } else {
          return item
        }
      })
      state.book = {} as Book
    },
    createBook (state, action) {
      state.books = [...state.books, action.payload]
      state.book = {} as Book
    },
    createAuthor (state, action) {
      state.authors = [...state.authors, action.payload]
    },
    deleteBook (state, action) {
      state.books = state.books.filter(({ id }) => id !== action.payload)
    },
    rentBook (state, action) {
      state.books = state.books.map((item) => {
        if (item.id === action.payload) {
          item.quantity = item.quantity - 1
          return item
        } else {
          return item
        }
      })
    }
  }
})

export const { getBooks, getBookById, getAuthors, updateBook, createBook, createAuthor, deleteBook, rentBook } = bookReducer.actions
export default bookReducer.reducer
