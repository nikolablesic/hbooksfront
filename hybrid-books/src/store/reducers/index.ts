import { combineReducers } from 'redux'
import AuthReducer from './auth.reducer'
import BookReducer from './book.reducer'

export default combineReducers({
  books: BookReducer,
  auth: AuthReducer
})
